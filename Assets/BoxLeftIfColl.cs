﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxLeftIfColl : MonoBehaviour
{
    public GameObject button;
    public bool once = false;

    private void Update()
    {
        if(GetComponent<BoxGoToPosition>().go && !once)
        {
            StartCoroutine(isColl());
            once = true;
        }
    }

    public IEnumerator isColl()
    {
        yield return new WaitForSeconds(2f);
        if (GetComponent<BoxColl>().coll == false)
        {
            button.GetComponent<Animator>().SetBool("show", true);
        }
        else
        {
            GetComponent<BoxColl>().coll = false;
        }
    }
}
