﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookColl : MonoBehaviour
{
    public GameObject point;
    public GameObject floatObject;
    public GameObject camera;

    private GameObject fishing;

    public GameObject button;

    public float howDeep = 0f;
    public float points = 0f;


    private void Start()
    {
        fishing = GameObject.Find("Fishing");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy" || collision.tag == "StoneDno")
        {
            GetComponent<HookMovement>().enabled = false;

            howDeep = Mathf.Abs(point.transform.position.y - transform.position.y);

            var f =Instantiate(floatObject, new Vector3(0f, -0.8f, -9f), Quaternion.identity);

            camera.GetComponent<CameraFollow>().target = f;

            fishing.GetComponent<Animator>().SetBool("show", true);
        }
        else if(collision.tag == "Fish")
        {
            Time.timeScale = 0f;
            button.GetComponent<Animator>().SetBool("show", true);
        }
    }

}
