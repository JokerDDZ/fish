﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFish : MonoBehaviour
{
    public GameObject fish;
    private float offset = 5f;

    public IEnumerator spawnFish()
    {
        yield return new WaitForSeconds(1f);

        GameObject p = GameObject.Find("Hook");

        float deepness = p.GetComponent<HookColl>().howDeep;

        for (int i = 0; i < deepness * 0.4f; i++)
        {
            float x = Random.Range(-4f, 4f);
            float y = Random.Range(-deepness + offset, -5f);

            var r = Instantiate(fish, new Vector3(x, y, -9f), Quaternion.identity);

            if(x > 0)
            {
                r.GetComponent<SpriteRenderer>().flipX = true;
            }
        }
    }
}
