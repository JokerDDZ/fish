﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxColl : MonoBehaviour
{
    public Sprite fish;
    private GameObject gm;

    public GameObject button;

    public bool coll = false;

    private void Start()
    {
        gm = GameObject.Find("GameManager");
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Box")
        {
            //GetComponent<BoxGoToPosition>().enabled = false;

            GameObject p = GameObject.Find("Hook");
            p.GetComponent<SpriteRenderer>().sprite = fish;
            GameObject camera = GameObject.Find("Main Camera");
            camera.GetComponent<CameraFollow>().target = p;
            p.GetComponent<HookMovement>().enabled = true;

            StartCoroutine(p.GetComponent<HookMovement>().goUpWithDelay());
            StartCoroutine(gm.GetComponent<SpawnFish>().spawnFish());

           transform.GetComponentInParent<Animator>().SetBool("show", false);

            coll = true;
        }
    }

}
