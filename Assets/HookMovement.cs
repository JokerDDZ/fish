﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HookMovement : MonoBehaviour
{
    private float velocity = 5f;
    private float velocitDown = 1.1f;
    private float borders = 2.6f;

    private int numberOfPoints = 0;

    public GameObject textPoints;
    private GameObject gm;

    public Sprite hookSprite;
    public bool up = false;
    public GameObject fishing;

    private void Start()
    {
        gm = GameObject.Find("GameManager");
    }

    private void FixedUpdate()
    {
        float movementDirX = Input.GetAxisRaw("Horizontal");
        Vector2 movementDir;

        if (!up)
        {
            movementDir = new Vector2(movementDirX, -velocitDown);
        }
        else
        {
            movementDir = new Vector2(movementDirX, velocitDown);
        }
        

        transform.Translate(movementDir * velocity * Time.deltaTime);

        if(transform.position.x > borders)
        {
            transform.position = new Vector3(2.6f, transform.position.y, -9f);
        }

        if (transform.position.x < -borders)
        {
            transform.position = new Vector3(-2.6f, transform.position.y, -9f);
        }

        if(transform.position.y > 2.5f)
        {
            enabled = false;

            numberOfPoints += (int)GetComponent<HookColl>().points;

            textPoints.GetComponent<Text>().text = numberOfPoints.ToString();

            StartCoroutine(startAgain());
        }
    }


    public IEnumerator goUpWithDelay()
    {
        float handlerV = velocitDown;
        velocitDown = 0f;

        yield return new WaitForSeconds(2f);

        up = true;
        velocitDown = handlerV;
    }

    public IEnumerator startAgain()
    {
        yield return new WaitForSeconds(2f);

        var enemys = GameObject.FindGameObjectsWithTag("Enemy");

        foreach(GameObject enemy in enemys)
        {
            Destroy(enemy.gameObject);
        }

        var fish = GameObject.FindGameObjectsWithTag("Fish");

        foreach(GameObject f in fish)
        {
            Destroy(f.gameObject);
        }

        gm.GetComponent<SpawnStones>().goSpawnStones();

        GetComponent<SpriteRenderer>().sprite = hookSprite;

        up = false;
        enabled = true;

        fishing.transform.GetChild(1).transform.position = new Vector3(2.3f, 3.155f, -9f);
        fishing.transform.GetChild(2).transform.position = new Vector3(-2.3f, 3.155f, -9f);

        fishing.transform.GetChild(2).GetComponent<BoxCollider2D>().enabled = false;
        fishing.transform.GetChild(2).GetComponent<BoxLeftIfColl>().once = false;
    }
}
