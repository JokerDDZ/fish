﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingRodMovement : MonoBehaviour
{
    private GameObject hook;

    private void Start()
    {
        hook = GameObject.Find("Hook");
    }

    private void FixedUpdate()
    {
        transform.position = new Vector3(hook.transform.position.x, transform.position.y, -9f);
    }
}
