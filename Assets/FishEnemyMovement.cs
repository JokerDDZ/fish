﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishEnemyMovement : MonoBehaviour
{
    public Vector3 dir = new Vector3(1f, 0f, 0f);
    private float velocity = 0.5f;

    private void FixedUpdate()
    {
        if(!GetComponent<SpriteRenderer>().flipX)
        {
            transform.Translate(dir * velocity * Time.deltaTime);
        }
        else
        {
            transform.Translate(-dir * velocity * Time.deltaTime);
        }
        
    }
}
