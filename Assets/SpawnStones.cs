﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStones : MonoBehaviour
{
    public GameObject stone;

    void Start()
    {
        goSpawnStones();
    }

    public void goSpawnStones()
    {
        for (int i = 0; i < 25; i++)
        {
            bool allGood = false;

            while (!allGood)
            {
                float x = Random.Range(-2.95f, 2.95f);
                float y = Random.Range(-7f, -66f);

                GameObject s = Instantiate(stone, new Vector3(x, y, -9f), Quaternion.identity);

                var enemys = GameObject.FindGameObjectsWithTag("Enemy");

                allGood = true;

                foreach (GameObject enemy in enemys)
                {
                    if (enemy != s.gameObject)
                    {
                        var dis = Vector3.Distance(s.gameObject.transform.position, enemy.transform.position);

                        if (dis < 1.5f)
                        {
                            allGood = false;
                            Destroy(s.gameObject);
                            break;
                        }
                    }
                }
            }

        }
    }

}
