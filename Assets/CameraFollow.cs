﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject target;
    private float smoothSpeed = 0.3f;
    private Vector3 offset = new Vector3(0f, 0f, -10f);

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Hook");
    }

    private void FixedUpdate()
    {
        Vector2 myPosition = new Vector2(transform.position.x, transform.position.y);
        Vector2 playerPosition = new Vector2(target.transform.position.x, target.transform.position.y);

        Vector3 smoothPostion = transform.position = Vector2.Lerp(myPosition, new Vector2(myPosition.x,playerPosition.y), smoothSpeed);

        transform.position = smoothPostion + offset;
    }

}
