﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateGo : MonoBehaviour
{
    private GameObject gm;

    private void Start()
    {
        gm = GameObject.Find("GameManager");
    }


    public void activateGo()
    {
        transform.GetChild(1).GetComponent<BoxGoToPosition>().enabled = true;
        transform.GetChild(2).GetComponent<BoxGoToPosition>().enabled = true;

        transform.GetChild(1).GetComponent<BoxGoToPosition>().go = true;
        transform.GetChild(2).GetComponent<BoxGoToPosition>().go = true;

        var enemys = GameObject.FindGameObjectsWithTag("Enemy");

        foreach(GameObject enemy in enemys)
        {
            Destroy(enemy.gameObject);
        }

        GameObject p = GameObject.Find("Hook");

        var hDeep =  p.GetComponent<HookColl>().howDeep;

        if(hDeep > 46f)
        {
            p.GetComponent<SpriteRenderer>().color = new Color(1f, 0.9665769f, 0f);
            p.GetComponent<HookColl>().points = 300f;
        }
        else if(hDeep > 26f)
        {
            p.GetComponent<SpriteRenderer>().color = new Color(0.9345012f, 0, 1f);
            p.GetComponent<HookColl>().points = 200f;
        }
        else
        {
            p.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f);
            p.GetComponent<HookColl>().points = 100f;
        }

    }
}
