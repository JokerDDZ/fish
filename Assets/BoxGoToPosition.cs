﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxGoToPosition : MonoBehaviour
{
    public Vector3 posToGo = new Vector3(0f, 0f, -9f);
    private float speed = 5f;

    public bool go = false;

    public Sprite fish;

    private void Update()
    {
        if(Input.GetKeyDown("space") && go)
        {
            GetComponent<BoxCollider2D>().enabled = true;
            go = false;         
        }

    }

    private void FixedUpdate()
    {
        if(go)
        {
            transform.position = Vector2.MoveTowards(transform.position, posToGo, speed * Time.deltaTime);
        }
        
    }

}
