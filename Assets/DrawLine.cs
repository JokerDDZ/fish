﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{
    public GameObject gameObject1;          
    public GameObject gameObject2;          

    private LineRenderer line;                          

    // Use this for initialization
    void Start()
    {

        line = GetComponent<LineRenderer>();
        line.SetWidth(0.05F, 0.05F);
        line.SetVertexCount(2);

        line.startColor = new Color(0f, 0f, 0f);
        line.sortingOrder = 5;
    }

    // Update is called once per frame
    void Update()
    {

        if (gameObject1 != null && gameObject2 != null)
        {
            line.SetPosition(0, gameObject1.transform.position);
            line.SetPosition(1, gameObject2.transform.position);
        }
    }
}
